package VodenjeRacunov;

public interface CompaniesDao extends DaoCrud<Podjetje>
{
    Podjetje getByDavcnaStevilka(String davcna);
}
