package VodenjeRacunov;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Racun implements Searchable
{
    private UUID id;
    private Date datum;
    private Artikli seznam;
    private InternalArticleList seznamInternalArticle;
    private Podjetje podjetje;
    private BigDecimal končnaCena;
    private BigDecimal končnaCenaZDDV;
    private String davcnaStevilka;
    private String davcnaStevilkaKupca;
    Kupon kupon;
    private UUID IDpodjetje;

    SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("hhmm");
    SimpleDateFormat secondFormat = new SimpleDateFormat("ssss");
    SimpleDateFormat millisecondFormat = new SimpleDateFormat("S");

    public Racun(Date datum, Artikli seznam, Podjetje podjetje, String davcna)
    {


        this.id = UUID.fromString(dateFormat.format(datum)+"-"+timeFormat.format(datum)+"-"+secondFormat.format(datum)+"-b000-000000"+millisecondFormat.format(datum));
        this.datum = datum;
        this.seznam = seznam;
        this.podjetje = podjetje;
        this.davcnaStevilka = "SI"+davcna;
        this.davcnaStevilkaKupca = "";
    }

    public Racun() {
    }

    public Racun(Artikli seznam, InternalArticleList seznamInternal, Podjetje podjetje)
    {
        Date date = new Date();
        this.id = UUID.fromString(dateFormat.format(date)+"-"+timeFormat.format(date)+"-"+secondFormat.format(date)+"-b000-000000"+millisecondFormat.format(date));
        this.datum = date;
        this.seznam = seznam;
        this.seznamInternalArticle = seznamInternal;
        this.končnaCena = this.generirajCeno();
        this.končnaCenaZDDV = this.generirajCenoZDdv();
        this.podjetje = podjetje;
        this.davcnaStevilka = podjetje.getDavcnaSt();
        this.IDpodjetje = podjetje.getId();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Artikli getSeznam() {
        return seznam;
    }

    public void setSeznam(Artikli seznam) {
        this.seznam = seznam;
    }

    public Podjetje getPodjetje() {
        return podjetje;
    }

    public void setPodjetje(Podjetje podjetje) {
        this.podjetje = podjetje;
    }

    public String getDavcnaStevilka() {
        return davcnaStevilka;
    }

    public void setDavcnaStevilka(String davcnaStevilka) {
        this.davcnaStevilka = davcnaStevilka;
    }

    public String getDavcnaStevilkaKupca() {
        return davcnaStevilkaKupca;
    }

    public void setDavcnaStevilkaKupca(String davcnaStevilkaKupca) {
        this.davcnaStevilkaKupca = davcnaStevilkaKupca;
    }

    public BigDecimal getKončnaCena() {
        return končnaCena;
    }

    public void setKončnaCena(BigDecimal končnaCena) {
        this.končnaCena = končnaCena;
    }

    public BigDecimal getKončnaCenaZDDV() {
        return končnaCenaZDDV;
    }

    public void setKončnaCenaZDDV(BigDecimal končnaCenaZDDV) {
        this.končnaCenaZDDV = končnaCenaZDDV;
    }

    public InternalArticleList getSeznamInternalArticle() {
        return seznamInternalArticle;
    }

    public void setSeznamInternalArticle(InternalArticleList seznamInternalArticle) {
        this.seznamInternalArticle = seznamInternalArticle;
    }

    public UUID getIDpodjetje()
    {
        return this.IDpodjetje;
    }

    public void setIDpodjetje(UUID id)
    {
        this.IDpodjetje = id;
    }

    public BigDecimal generirajCeno()
    {
        BigDecimal skupajCena = new BigDecimal("0");

        for(Artikel a: this.seznam.getSeznamArtiklov())
        {
            for(int i=0; i<a.getKolicina(); i++)
            {
                skupajCena = skupajCena.add(a.getCena());
            }
        }
        return skupajCena;
    }

    public BigDecimal generirajCenoZDdv()
    {
        BigDecimal skupajCena = new BigDecimal("0");
        BigDecimal sto = new BigDecimal("100");
        BigDecimal nizka = new BigDecimal("9.5");
        BigDecimal visoka = new BigDecimal("22");

        for(Artikel a: this.seznam.getSeznamArtiklov())
        {
            for(int i=0; i<a.getKolicina(); i++)
            {
                if(a.getDavcnaStopnja() == 9.5)
                {
                    BigDecimal temp = new BigDecimal("0");
                    temp = a.getCena();
                    temp = temp.multiply(nizka);
                    temp = temp.divide(sto, 2, RoundingMode.HALF_UP);
                    temp = temp.add(a.getCena());
                    skupajCena = skupajCena.add(temp);
                }
                else
                {
                    BigDecimal temp = new BigDecimal("0");
                    temp = a.getCena();
                    temp = temp.multiply(visoka);
                    temp = temp.divide(sto, 2, RoundingMode.HALF_UP);
                    temp = temp.add(a.getCena());
                    skupajCena = skupajCena.add(temp);
                }
            }
        }
        return skupajCena;
    }

    public void printajRacun()
    {
        System.out.println("---------------------------------------------------");
        System.out.println("Izdelek              Kolicina    Cena za kos    DDV");

        for(Artikel a: this.getSeznam().getSeznamArtiklov())
        {

            System.out.print(a.getIme());
            for(int i=a.getIme().length(); i<27; i++)
            {
                System.out.print(" ");
            }

            System.out.print(a.getKolicina());
            System.out.print("           ");
            System.out.print(a.getCena());
            System.out.print("     ");
            System.out.println(a.getDavcnaStopnja());
        }
        System.out.println();
        System.out.println("Skupaj cena brez DDV: "+končnaCena+"€");
        System.out.println("Skupaj cena z DDV: "+končnaCenaZDDV+"€\n");

        SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        System.out.println("Datum: "+dt.format(this.getDatum()));

        System.out.println("ID racuna: "+this.getId().toString());
        System.out.println("Izdajatelj: "+this.getPodjetje().getIme());
        System.out.println("Davčna stevilka: "+this.getDavcnaStevilka());
        if(!aliJeDavcniZavezanec(this.podjetje))
        {
            System.out.println("Ni davcni zavezanec");
        }
        else
        {
            System.out.println("Je davcni zavezanec");
        }
        System.out.println("---------------------------------------------------");
    }

    @Override
    public boolean search(String s)
    {
        if(this.id.toString().contains(s))
        {
            return true;
        }
        else if(this.datum.toString().contains(s))
        {
            return true;
        }
        else if(this.davcnaStevilka.contains(s))
        {
            return true;
        }
        else if(this.podjetje.getIme().contains(s))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean aliJeDavcniZavezanec(Podjetje podjetje)
    {
        if(this.podjetje.isJeDavcniZavezanec())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void uporabiKupon(Kupon kupon) throws ParseException {
        String EAN = kupon.getEAN();

        int kolicina;
        SimpleDateFormat datumFormat = new SimpleDateFormat("ddMMyyyy");
        Date datumKupona;
        Date datumRačuna;

        if(EAN.substring(0,2).equals("99"))
        {
            kolicina = Integer.parseInt(EAN.substring(2, 4));
            datumKupona = datumFormat.parse(EAN.substring(4, 12));
            datumRačuna = this.getDatum();

            if(datumRačuna.compareTo(datumKupona)>0)
            {
                System.out.println("Kupon je že potekel!");
            }
            else if(datumRačuna.compareTo(datumKupona)<=0)
            {
                BigDecimal temp = new BigDecimal(kolicina);
                BigDecimal sto = new BigDecimal("100");
                BigDecimal ena = new BigDecimal("1");
                temp = temp.divide(sto, 2, RoundingMode.UP);
                temp = ena.subtract(temp);
                končnaCenaZDDV = končnaCenaZDDV.multiply(temp);
                končnaCena = končnaCena.multiply(temp);
            }
        }
        else
        {
            System.out.println("To ni kupon!");
        }

    }
}
