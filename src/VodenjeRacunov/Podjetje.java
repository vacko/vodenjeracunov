package VodenjeRacunov;

import java.util.UUID;

public class Podjetje implements Searchable
{
    private UUID id;
    private String ime;
    private String davcnaSt;
    private String maticnaSt;
    private boolean jeDavcniZavezanec;
    private String tel;
    private String naslov;

    public Podjetje(String ime, String davcnaSt, String maticnaSt, boolean jeDavcniZavezanec, String tel, String naslov) {
        this.id = UUID.randomUUID();
        this.ime = ime;
        this.davcnaSt = davcnaSt;
        this.maticnaSt = maticnaSt;
        this.jeDavcniZavezanec = jeDavcniZavezanec;
        this.tel = tel;
        this.naslov = naslov;
    }

    public Podjetje() {
    }

    public Podjetje(Podjetje p) {
        this.id = p.getId();
        this.ime = p.getIme();
        this.davcnaSt = p.getDavcnaSt();
        this.maticnaSt = p.getMaticnaSt();
        this.jeDavcniZavezanec = p.isJeDavcniZavezanec();
        this.tel = p.getTel();
        this.naslov = p.getNaslov();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getDavcnaSt() {
        return davcnaSt;
    }

    public void setDavcnaSt(String davcnaSt) {
        this.davcnaSt = davcnaSt;
    }

    public String getMaticnaSt() {
        return maticnaSt;
    }

    public void setMaticnaSt(String maticnaSt) {
        this.maticnaSt = maticnaSt;
    }

    public boolean isJeDavcniZavezanec() {
        return jeDavcniZavezanec;
    }

    public void setJeDavcniZavezanec(boolean jeDavcniZavezanec) {
        this.jeDavcniZavezanec = jeDavcniZavezanec;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    @Override
    public String toString() {
        return "Podjetje{" +
                "ime='" + ime + '\'' +
                ", davčnaŠt='" + davcnaSt + '\'' +
                ", matičnaŠt='" + maticnaSt + '\'' +
                ", jeDavčniZavezanec=" + jeDavcniZavezanec +
                '}';
    }

    @Override
    public boolean search(String s)
    {
        if(this.getIme().contains(s))
        {
            return true;
        }
        else if(this.getDavcnaSt().contains(s))
        {
            return true;
        }
        else if(this.getMaticnaSt().equals(s))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
