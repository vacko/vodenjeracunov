package si.um.feri.database;

import java.nio.ByteBuffer;
import java.util.UUID;

public class UUIDadapter
{
    public static byte[] getBytesFromUUID(UUID uuid)
    {
        ByteBuffer buffer = ByteBuffer.wrap(new byte[16]);
        buffer.putLong(uuid.getMostSignificantBits());
        buffer.putLong(uuid.getLeastSignificantBits());

        return buffer.array();
    }

    public static UUID getUUIDFromBytes(byte[] bytes)
    {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        Long high = buffer.getLong();
        Long low = buffer.getLong();

        UUID uuid = new UUID(high, low);
        return uuid;
    }
}
