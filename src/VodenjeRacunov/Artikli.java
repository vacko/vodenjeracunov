package VodenjeRacunov;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Artikli implements JsonSupport
{
    private List<Artikel> seznamArtiklov;

    public Artikli()
    {
        this.seznamArtiklov = new ArrayList<>();
    }

    public Artikli(List<Artikel> list)
    {
        this.seznamArtiklov = list;
    }

    public List<Artikel> getSeznamArtiklov()
    {
        return this.seznamArtiklov;
    }

    public void dodajArtikel(Artikel a)
    {
        int i=0;

        for(Artikel artikel: this.seznamArtiklov)
        {
            if(artikel.getIme().equals(a.getIme()))
            {
                artikel.povecajKolicino(1);
                i=1;
                break;
            }
        }
        if(i==0)
        {
            this.seznamArtiklov.add(a);
        }
    }

    public void odstraniZadnjega()
    {
        this.seznamArtiklov.remove(this.seznamArtiklov.size()-1);
    }

    public void odstrani(int index)
    {
        if(index>this.seznamArtiklov.size()-1)
        {
            System.out.println("This item does not exist!");
        }
        else
        {
            this.seznamArtiklov.remove(index);
        }
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }

    @Override
    public void fromJson(String s) {
        Gson gson = new Gson();
        Artikli a = gson.fromJson(s, Artikli.class);
        seznamArtiklov = a.seznamArtiklov;
    }
}
