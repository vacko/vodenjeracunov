package VodenjeRacunov;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Invoices implements JsonSupport
{
    private List<Racun> seznam;

    public Invoices()
    {
        this.seznam = new ArrayList<>();
    }

    public List<Racun> getSeznamRacunov() {
        return this.seznam;
    }

    public List<Racun> getSeznam() {
        return seznam;
    }

    public void setSeznam(List<Racun> seznam) {
        this.seznam = seznam;
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }

    @Override
    public void fromJson(String s) {
        Gson gson = new Gson();
        Invoices n = gson.fromJson(s, Invoices.class);
        seznam = n.seznam;
    }
}
