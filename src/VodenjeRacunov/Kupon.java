package VodenjeRacunov;

public class Kupon
{
    private String EAN;//2-kupon,2-količina, 8-datum izteka, 1-check digit
    private String ime;

    public Kupon(String EAN, String ime) {
        this.EAN = EAN;
        this.ime = ime;
    }

    public String getEAN() {
        return EAN;
    }

    public void setEAN(String EAN) {
        this.EAN = EAN;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }
}
