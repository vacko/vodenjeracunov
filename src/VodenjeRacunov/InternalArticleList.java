package VodenjeRacunov;

import java.util.ArrayList;
import java.util.List;

public class InternalArticleList
{
    private List<InternalArticle> list;

    public InternalArticleList(List<InternalArticle> list) {
        this.list = list;
    }

    public InternalArticleList()
    {
        list = new ArrayList<>();
    }

    public List<InternalArticle> getList() {
        return list;
    }

    public void setList(List<InternalArticle> list) {
        this.list = list;
    }

    public void dodajArtikel(InternalArticle ia)
    {
        int i=0;

        for(InternalArticle artikel: this.list)
        {
            if(artikel.getId().equals(ia.getId()))
            {
                artikel.povecajKolicino(1);
                i=1;
                break;
            }
        }

        if(i == 0)
        {
            this.list.add(ia);
        }
    }

    public void odstraniArtikel(InternalArticle ai)
    {
        int i=0;

        for (InternalArticle artikel: this.list)
        {
            if(artikel.getInternal_id().equals(ai.getInternal_id()))
            {
                if(artikel.getStock() > 1)
                {
                    artikel.povecajKolicino(-1);
                    i=1;
                    break;
                }
                else
                {
                    this.list.remove(artikel);
                    break;
                }
            }
        }

        if(i == 0)
        {
            this.list.remove(ai);
        }
    }
}
