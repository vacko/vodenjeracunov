package VodenjeRacunov;

import org.apache.commons.dbcp2.BasicDataSource;
import si.um.feri.database.DBHelper;
import si.um.feri.database.UUIDadapter;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MySqlInternalArticle implements InternalArticleDao
{
    final String SQL_GET_BY_ID_ARTICLE = "SELECT * FROM `internalarticle` WHERE `internal_id`=?";
    final String SQL_GET_BY_ID = "SELECT * FROM `internalarticle` WHERE `internal_article_id`=?";
    final String SQL_GET_ALL = "SELECT * FROM `internalarticle`";
    final String SQL_INSERT = "INSERT INTO `internalarticle`(`internal_article_id`,`barcode`,`name`,`price`,`vat`,`stock`,`deleted`,`created`) VALUES(?,?,?,?,?,?,?,?)";
    final String SQL_UPDATE = "UPDATE `internalarticle` SET `internal_id`=?, `name`=?, `price`=?, `vat`=?, `stock`=? WHERE `internal_article_id`=?;";
    final String SQL_DELETE = "DELETE FROM `internalarticle` WHERE `internal_article_id`=?";

    @Override
    public InternalArticle getById(String id) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_BY_ID_ARTICLE);
            prepStat.setString(1, id);

            ResultSet resSet = prepStat.executeQuery();

            if(resSet.first())
            {
                return extractFromResultSet(resSet);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public InternalArticle getById(UUID id) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_BY_ID);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(id));

            ResultSet resSet = prepStat.executeQuery();

            if(resSet.first())
            {
                return extractFromResultSet(resSet);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<InternalArticle> getAll() {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_ALL);

            ResultSet resSet = prepStat.executeQuery();
            List<InternalArticle> list = new ArrayList<>();

            try(ResultSet resultSet = prepStat.executeQuery();)
            {
                while (resultSet.next())
                {
                    list.add(extractFromResultSet(resultSet));
                }
                return list;
            }
            catch (Exception e)
            {
                connection.rollback();
                e.printStackTrace();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insert(InternalArticle m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_INSERT);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(m.getId()));
            prepStat.setString(2, m.getInternal_id());
            prepStat.setString(3, m.getName());
            prepStat.setBigDecimal(4, m.getPrice());
            prepStat.setDouble(5, m.getVat());
            prepStat.setInt(6, m.getStock());
            prepStat.setInt(7, 0);
            prepStat.setTimestamp(8, new Timestamp(System.currentTimeMillis()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(InternalArticle m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_UPDATE);
            prepStat.setString(1, m.getInternal_id());
            prepStat.setString(2, m.getName());
            prepStat.setBigDecimal(3, m.getPrice());
            prepStat.setDouble(4, m.getVat());
            prepStat.setInt(5, m.getStock());
            prepStat.setBytes(6, UUIDadapter.getBytesFromUUID(m.getId()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean delete(InternalArticle m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_DELETE);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(m.getId()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public InternalArticle extractFromResultSet(ResultSet rs) throws SQLException {

        InternalArticle a = new InternalArticle();
        a.setId(UUID.nameUUIDFromBytes(rs.getBytes(1)));
        a.setInternal_id(rs.getString(2));
        a.setName(rs.getString(3));
        a.setPrice(rs.getBigDecimal(4));
        a.setVat(rs.getDouble(5));
        a.setStock(rs.getInt(6));

        return a;
    }
}
