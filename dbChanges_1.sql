-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema InvoiceManagment_dev
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema InvoiceManagment_dev
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `InvoiceManagment_dev` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `InvoiceManagment_dev` ;

-- -----------------------------------------------------
-- Table `InvoiceManagment_dev`.`SchemaVersion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `InvoiceManagment_dev`.`SchemaVersion` (
  `schema_version_id` BINARY(16) NOT NULL,
  `change_number` INT NOT NULL,
  `version` VARCHAR(30) NOT NULL,
  `applied` DATETIME NOT NULL,
  `applied_by` VARCHAR(45) NOT NULL,
  `description` VARCHAR(300) NOT NULL,
  `file` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`schema_version_id`))
ENGINE = InnoDB;

-- SchemaVersion inicialization?!?!
SET @change_number=0;
SET @version=1;
SET @applied=NOW();
SET @applied_by="Tadej";
SET @description="Test";
SET @file="dbChanges_1.sql";

INSERT INTO `invoicemanagment_dev`.`schemaversion`(`schema_version_id`,`change_number`,`version`,`applied`,`applied_by`,`description`,`file`)
values(unhex(replace(UUID(),"-","")), @change_number, @version, @applied, @applied_by, @description, @file);


-- -----------------------------------------------------
-- Table `InvoiceManagment_dev`.`Article`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `InvoiceManagment_dev`.`Article` (
  `article_id` BINARY(16) NOT NULL,
  `barcode` VARCHAR(14) NOT NULL,
  `name` VARCHAR(1000) NOT NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `vat` DECIMAL(5,2) NOT NULL,
  `stock` INT NOT NULL,
  `deleted` SMALLINT(1) NOT NULL DEFAULT false,
  `created` DATETIME NOT NULL,
  `last_modified` DATETIME NULL,
  PRIMARY KEY (`article_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `InvoiceManagment_dev`.`InternalArticle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `InvoiceManagment_dev`.`InternalArticle` (
  `internal_article_id` BINARY(16) NOT NULL,
  `internal_id` VARCHAR(4) NOT NULL,
  `name` VARCHAR(1000) NOT NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `vat` DECIMAL(5,2) NOT NULL,
  `stock` INT NOT NULL,
  `deleted` TINYINT(1) NOT NULL DEFAULT false,
  `created` DATETIME NOT NULL,
  `last_modified` DATETIME NULL,
  PRIMARY KEY (`internal_article_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `InvoiceManagment_dev`.`Company`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `InvoiceManagment_dev`.`Company` (
  `company_id` BINARY(16) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `tax_number` VARCHAR(45) NOT NULL,
  `registration_number` VARCHAR(45) NOT NULL,
  `phone_number` VARCHAR(30) NOT NULL,
  `taxpayer` TINYINT(1) NOT NULL,
  `address` VARCHAR(150) NOT NULL,
  `deleted` TINYINT(1) NOT NULL DEFAULT false,
  `created` DATETIME NOT NULL,
  `last_modified` DATETIME NULL,
  PRIMARY KEY (`company_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `InvoiceManagment_dev`.`Invoice`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `InvoiceManagment_dev`.`Invoice` (
  `invoice_id` BINARY(16) NOT NULL,
  `total` DECIMAL(10,2) NOT NULL,
  `total_vat` DECIMAL(10,2) NOT NULL,
  `deleted` TINYINT(1) NOT NULL DEFAULT false,
  `created` DATETIME NOT NULL,
  `last_modified` DATETIME NULL,
  `isuser_id` BINARY(16) NOT NULL,
  `customer_id` BINARY(16) NULL,
  PRIMARY KEY (`invoice_id`),
  INDEX `fk_Invoice_Company1_idx` (`isuser_id` ASC) VISIBLE,
  INDEX `fk_Invoice_Company2_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `fk_Invoice_Company1`
    FOREIGN KEY (`isuser_id`)
    REFERENCES `InvoiceManagment_dev`.`Company` (`company_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Invoice_Company2`
    FOREIGN KEY (`customer_id`)
    REFERENCES `InvoiceManagment_dev`.`Company` (`company_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `InvoiceManagment_dev`.`Article_has_Invoice`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `InvoiceManagment_dev`.`Article_has_Invoice` (
  `article_id` BINARY(16) NOT NULL,
  `invoice_id` BINARY(16) NOT NULL,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`article_id`, `invoice_id`),
  INDEX `fk_Article_has_Invoice_Invoice1_idx` (`invoice_id` ASC) VISIBLE,
  INDEX `fk_Article_has_Invoice_Article_idx` (`article_id` ASC) VISIBLE,
  CONSTRAINT `fk_Article_has_Invoice_Article`
    FOREIGN KEY (`article_id`)
    REFERENCES `InvoiceManagment_dev`.`Article` (`article_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Article_has_Invoice_Invoice1`
    FOREIGN KEY (`invoice_id`)
    REFERENCES `InvoiceManagment_dev`.`Invoice` (`invoice_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `InvoiceManagment_dev`.`Invoice_has_InternalArticle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `InvoiceManagment_dev`.`Invoice_has_InternalArticle` (
  `invoice_id` BINARY(16) NOT NULL,
  `internal_article_id` BINARY(16) NOT NULL,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`invoice_id`, `internal_article_id`),
  INDEX `fk_Invoice_has_InternalArticle_InternalArticle1_idx` (`internal_article_id` ASC) VISIBLE,
  INDEX `fk_Invoice_has_InternalArticle_Invoice1_idx` (`invoice_id` ASC) VISIBLE,
  CONSTRAINT `fk_Invoice_has_InternalArticle_Invoice1`
    FOREIGN KEY (`invoice_id`)
    REFERENCES `InvoiceManagment_dev`.`Invoice` (`invoice_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Invoice_has_InternalArticle_InternalArticle1`
    FOREIGN KEY (`internal_article_id`)
    REFERENCES `InvoiceManagment_dev`.`InternalArticle` (`internal_article_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

delimiter //
CREATE TRIGGER `modifiedArticle`
BEFORE UPDATE ON `InvoiceManager_dev`.`Article`
FOR EACH ROW BEGIN
	SET new.`last_modified` = NOW();
END
delimiter ;

delimiter //
CREATE TRIGGER `modifiedInternalArticle`
BEFORE UPDATE ON `InvoiceManager_dev`.`InternalArticle`
FOR EACH ROW BEGIN
	SET new.`last_modified` = NOW();
END
delimiter ;

delimiter //
CREATE TRIGGER `modifiedCompany`
BEFORE UPDATE ON `InvoiceManager_dev`.`Company`
FOR EACH ROW BEGIN
	SET new.`last_modified` = NOW();
END
delimiter ;

delimiter //
CREATE TRIGGER `modifiedInvoice`
BEFORE UPDATE ON `InvoiceManager_dev`.`Invoice`
FOR EACH ROW BEGIN
	SET new.`last_modified` = NOW();
END
delimiter ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;