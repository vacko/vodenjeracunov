package si.um.feri.database;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.*;
import java.util.UUID;


import VodenjeRacunov.Artikel;
import com.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.dbcp2.BasicDataSource;


public class DBHelper
{
    private static BasicDataSource dataSource;
    private static Gson podatki;
    private Povezava connection;

    public static class Povezava
    {
        private String username;
        private String password;
        private String connection;

        public Povezava(String connection, String username, String password) {
            this.connection = connection;
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getConnection() {
            return connection;
        }

        public void setConnection(String connection) {
            this.connection = connection;
        }

        public static Povezava getPovezava()
        {
            try
            {
                BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Users\\Tadej\\Desktop\\Oddane naloge\\2.letnik\\Programski jeziki\\datoteke\\povezava.json"));
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                Povezava connection = gson.fromJson(bufferedReader, Povezava.class);

                return connection;
            }
            catch (Exception e)
            {
                System.out.println("Napaka pri pridobivanju povezave!\n"+e.toString());
            }
            return null;
        }
    }

    private static BasicDataSource getDataSource()
    {
        Povezava connect = Povezava.getPovezava();

        if(dataSource == null)
        {
            BasicDataSource dataS = new BasicDataSource();
            dataS.setUrl(connect.getConnection());
            dataS.setUsername(connect.getUsername());
            dataS.setPassword(connect.getPassword());

            dataS.setMinIdle(5);
            dataS.setMaxIdle(10);
            dataS.setMaxOpenPreparedStatements(100);

            dataSource = dataS;
        }
        return dataSource;
    }

    public static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    public static void insertDataCSV(String file, char locilo)
    {
        int indexName=0, indexBarcode=0;
        String[] rowData = null;

        if(locilo == ',')
        {
            indexBarcode = 1;
            indexName = 4;
        }
        else if(locilo == '\t')
        {
            indexBarcode = 0;
            indexName = 7;
        }
        else
        {
            System.out.println("Napačno ločilo!");
            return;
        }

        try(CSVReader reader = new CSVReader(new FileReader(file), locilo);
            Connection connect = DBHelper.getConnection())
        {
            String s = "INSERT INTO article(article_id, barcode, name, price, vat, stock, deleted, created) VALUES(?,?,?,?,?,?,?,?)";
            PreparedStatement prepStatement = connect.prepareStatement(s);
            connect.setAutoCommit(false);

            int i=0;
            rowData = reader.readNext();

            while((rowData = reader.readNext()) != null)
            {
                if(rowData[indexBarcode].length() <= 14)
                {
                    String rowBarcode = rowData[indexBarcode];
                    if(rowData[indexBarcode].length() < 14)
                    {
                        rowBarcode = StringUtils.leftPad(rowData[indexBarcode], 14, '0');
                    }

                    char checkDigit = rowBarcode.charAt(rowBarcode.length()-1);
                    String barocodeWithoutCheckDigit  = rowBarcode.substring(0, rowBarcode.length()-1);

                    if(Artikel.checkDigit14(barocodeWithoutCheckDigit, Character.getNumericValue(checkDigit)))
                    {
                        double random = Math.random();
                        double tempRes = Math.floor(random*1000);
                        double priceRes = tempRes/(Math.random()*100);
                        double varRes = 9.5;

                        BigDecimal price = new BigDecimal(priceRes).setScale(2, RoundingMode.HALF_UP);
                        BigDecimal vat = new BigDecimal(varRes).setScale(2, RoundingMode.HALF_UP);

                        prepStatement.setBytes(1, UUIDadapter.getBytesFromUUID(UUID.randomUUID()));
                        prepStatement.setString(2, rowBarcode);
                        prepStatement.setString(3, rowData[indexName]);
                        prepStatement.setDouble(4, price.doubleValue());
                        prepStatement.setDouble(5, vat.doubleValue());
                        prepStatement.setInt(6, 1000);
                        prepStatement.setInt(7, 0);
                        prepStatement.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
                        System.out.println("     id: "+rowData[indexBarcode]+" barcode: "+rowBarcode+" ime: "+rowData[indexName]);
                        i++;
                        prepStatement.addBatch();

                        if(i == 1000)
                        {
                            i=0;
                            prepStatement.executeBatch();
                            connect.commit();
                        }
                    }
                    else
                    {
                        System.out.println("Artikel ima napačen checkDigit!");
                        System.out.println("     id: "+rowData[indexBarcode]+" barcode: "+rowBarcode+" ime: "+rowData[indexName]);
                    }
                }
                prepStatement.executeBatch();
                connect.commit();
            }
            connect.setAutoCommit(true);
        }
        catch (Exception e)
        {
            System.out.println("Napaka: "+e.toString());
        }
    }

    public ResultSet pridobiIzdelke()
    {
        try(BasicDataSource dataSource = DBHelper.getDataSource();
            Connection connection = dataSource.getConnection();
            PreparedStatement prepStatement = connection.prepareStatement("SELECT * FROM Article"))
        {
            System.out.println("The connection object is of class: "+connection.getClass());

            try(ResultSet resultSet = prepStatement.executeQuery();)
            {
                return resultSet;
            }
            catch (Exception e)
            {
                connection.rollback();
                e.printStackTrace();
            }
        }
        catch (Exception e)
        {
            System.out.println("Napaka: "+e.toString());
        }
        return null;
    }

}
