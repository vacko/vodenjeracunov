package VodenjeRacunov;

import java.sql.Timestamp;

public interface InvoiceDao extends DaoCrud<Racun>
{
    Racun getByTIMESTAMP(Timestamp s);
    void create_invoice(Racun r);
}
