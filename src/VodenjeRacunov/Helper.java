package VodenjeRacunov;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;

public class Helper
{
    public static void pisanjeVDatoteko(String input, String fileName)
    {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(input);
        try( BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))){
            writer.write(json);
        }
        catch (IOException e)
        {
            System.out.println("Napaka na pisanju!\n"+e.toString());
        }
    }

    public static String branjeIzDatoteke(String ime)
    {
        try(BufferedReader br = new BufferedReader(new FileReader(ime))){
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
           return sb.toString();
        }
        catch (IOException e)
        {
            System.out.println("Napaka pri branju!\n"+e.toString());
        }
        return null;
    }
}
