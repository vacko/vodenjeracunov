package VodenjeRacunov;

import org.apache.commons.dbcp2.BasicDataSource;
import si.um.feri.database.DBHelper;
import si.um.feri.database.UUIDadapter;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MySqlArticle implements ArticleDao
{
    final String SQL_GET_BY_BARCODE = "SELECT * FROM `article` WHERE `barcode`=?";
    final String SQL_GET_BY_ID = "SELECT * FROM `article` WHERE `article_id`=?";
    final String SQL_GET_ALL = "SELECT * FROM `article`";
    final String SQL_INSERT = "INSERT INTO `article`(`article_id`,`barcode`,`name`,`price`,`vat`,`stock`,`deleted`,`created`) VALUES(?,?,?,?,?,?,?,?,?)";
    final String SQL_UPDATE = "UPDATE `article` SET `barcode`=?, `name`=?, `price`=?, `vat`=?, `stock`=? WHERE `article_id`=?;";
    final String SQL_DELETE = "DELETE FROM `article` WHERE `article_id`=?";

    @Override
    public Artikel getByBarcode(String code) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_BY_BARCODE);
            prepStat.setString(1, code);

            ResultSet resSet = prepStat.executeQuery();

            if(resSet.first())
            {
                return extractFromResultSet(resSet);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Artikel getById(UUID id) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_BY_ID);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(id));

            ResultSet resSet = prepStat.executeQuery();

            if(resSet.first())
            {
                return extractFromResultSet(resSet);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Artikel> getAll() {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_ALL);

            ResultSet resSet = prepStat.executeQuery();
            List<Artikel> list = new ArrayList<>();

            try(ResultSet resultSet = prepStat.executeQuery();)
            {
                while (resultSet.next())
                {
                    list.add(extractFromResultSet(resultSet));
                }
                return list;
            }
            catch (Exception e)
            {
                connection.rollback();
                e.printStackTrace();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insert(Artikel m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_INSERT);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(m.getId()));
            prepStat.setString(2, m.getEAN());
            prepStat.setString(3, m.getIme());
            prepStat.setBigDecimal(4, m.getCena());
            prepStat.setDouble(5, m.getDavcnaStopnja());
            prepStat.setInt(6, m.getKolicina());
            prepStat.setInt(7, 0);
            prepStat.setTimestamp(8, new Timestamp(System.currentTimeMillis()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Artikel m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_UPDATE);
            prepStat.setString(1, m.getEAN());
            prepStat.setString(2, m.getIme());
            prepStat.setBigDecimal(3, m.getCena());
            prepStat.setDouble(4, m.getDavcnaStopnja());
            prepStat.setInt(5, m.getKolicina());
            prepStat.setBytes(6, UUIDadapter.getBytesFromUUID(m.getId()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean delete(Artikel m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_DELETE);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(m.getId()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public Artikel extractFromResultSet(ResultSet rs) throws SQLException {

        Artikel a = new Artikel();
        a.setId(UUID.nameUUIDFromBytes(rs.getBytes(1)));
        a.setEAN(rs.getString(2));
        a.setIme(rs.getString(3));
        a.setCena(rs.getBigDecimal(4));
        a.setDavcnaStopnja(rs.getDouble(5));
        a.setKolicina(rs.getInt(6));

        return a;
    }
}
