package VodenjeRacunov;

import org.apache.commons.dbcp2.BasicDataSource;
import si.um.feri.database.DBHelper;
import si.um.feri.database.UUIDadapter;

import javax.naming.PartialResultException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MySqlInvoice implements InvoiceDao
{
    final String SQL_GET_BY_TIMESTAMP = "SELECT * FROM `invoice` WHERE `created`=?";
    final String SQL_GET_BY_ID = "SELECT * FROM `invoice` WHERE `invoice_id`=unhex(replace('?', '-', ''))";
    final String SQL_GET_ALL = "SELECT * FROM `invoice`";
    final String SQL_INSERT = "INSERT INTO `invoice`(`invoice_id`,`total`,`total_vat`,`deleted`,`created`,`isuser_id`) VALUES(?,?,?,?,?,?)";
    final String SQL_UPDATE = "UPDATE `inovice` SET `total`=?, `total_vat`=? WHERE `invoice_id`=?;";
    final String SQL_DELETE = "DELETE FROM `invoice` WHERE `invoice_id`=unhex(replace('?', '-', ''));";
    final String SQL_Invoice_has_article = "INSERT INTO `invoice_has_article`(`invoice_id`,`article_id`,`quantity`) VALUES(?,?,?);";
    final String SQL_Invoice_has_Internalarticle = "INSERT INTO `invoice_has_InternalArticle`(`invoice_id`,`internal_article_id`,`quantity`) VALUES(?,?,?);";

    @Override
    public Racun getByTIMESTAMP(Timestamp ts) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_BY_TIMESTAMP);
            prepStat.setTimestamp(1, ts);

            ResultSet resSet = prepStat.executeQuery();

            if(resSet.first())
            {
                return extractFromResultSet(resSet);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Racun getById(UUID id) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_BY_ID);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(id));

            ResultSet resSet = prepStat.executeQuery();

            if(resSet.first())
            {
                return extractFromResultSet(resSet);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Racun> getAll() {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_ALL);

            ResultSet resSet = prepStat.executeQuery();
            List<Racun> list = new ArrayList<>();

            try(ResultSet resultSet = prepStat.executeQuery())
            {
                while (resultSet.next())
                {
                    list.add(extractFromResultSet(resultSet));
                }
                return list;
            }
            catch (Exception e)
            {
                connection.rollback();
                e.printStackTrace();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insert(Racun m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_INSERT);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(m.getId()));
            prepStat.setBigDecimal(2, m.generirajCeno());
            prepStat.setBigDecimal(3, m.generirajCenoZDdv());
            prepStat.setInt(4, 0);
            prepStat.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
            prepStat.setBytes(6, UUIDadapter.getBytesFromUUID(m.getIDpodjetje()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Racun m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_UPDATE);
            prepStat.setBigDecimal(1, m.generirajCeno());
            prepStat.setBigDecimal(2, m.generirajCenoZDdv());
            prepStat.setBytes(6, UUIDadapter.getBytesFromUUID(m.getId()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Racun m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_DELETE);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(m.getId()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Racun extractFromResultSet(ResultSet rs) throws SQLException {
        Racun racun = new Racun();

        racun.setId(UUID.nameUUIDFromBytes(rs.getBytes(1)));
        racun.setKončnaCena(rs.getBigDecimal(2));
        racun.setKončnaCenaZDDV(rs.getBigDecimal(3));
        racun.setDavcnaStevilka(rs.getString(4));
        racun.setIDpodjetje(UUID.nameUUIDFromBytes(rs.getBytes(5)));
        return racun;
    }

    @Override
    public void create_invoice(Racun r) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement insert = connection.prepareStatement(SQL_INSERT);
            PreparedStatement prepStatArticle = connection.prepareStatement(SQL_Invoice_has_article);
            PreparedStatement prepStatIntArticle = connection.prepareStatement(SQL_Invoice_has_Internalarticle);

            System.out.println(r.getIDpodjetje().toString());

            insert.setBytes(1, UUIDadapter.getBytesFromUUID(r.getId()));
            insert.setBigDecimal(2, r.generirajCeno());
            insert.setBigDecimal(3, r.generirajCenoZDdv());
            insert.setInt(4, 0);
            insert.setTimestamp(5, new Timestamp(r.getDatum().getTime()));
            insert.setBytes(6, UUIDadapter.getBytesFromUUID(r.getPodjetje().getId()));

            System.out.println(insert);

            insert.executeUpdate();

            Artikli artikli = r.getSeznam();
            InternalArticleList list = r.getSeznamInternalArticle();

            for(Artikel artikel: artikli.getSeznamArtiklov())
            {
                prepStatArticle.setBytes(1, UUIDadapter.getBytesFromUUID(r.getId()));
                prepStatArticle.setBytes(2, UUIDadapter.getBytesFromUUID(artikel.getId()));
                prepStatArticle.setInt(3, artikel.getKolicina());
                System.out.println(artikel.getIme()+" "+artikel.getKolicina());
                prepStatArticle.executeUpdate();
            }

            for(InternalArticle internalArticle: list.getList())
            {
                prepStatIntArticle.setBytes(1, UUIDadapter.getBytesFromUUID(r.getId()));
                prepStatIntArticle.setBytes(2, UUIDadapter.getBytesFromUUID(internalArticle.getId()));
                prepStatIntArticle.setInt(3, internalArticle.getStock());
                prepStatIntArticle.executeUpdate();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
