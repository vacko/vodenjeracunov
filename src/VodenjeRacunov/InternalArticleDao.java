package VodenjeRacunov;

public interface InternalArticleDao extends DaoCrud<InternalArticle>{
    InternalArticle getById(String id);
}
