package VodenjeRacunov;

import si.um.feri.database.UUIDadapter;

import javax.naming.InterruptedNamingException;
import java.math.BigDecimal;
import java.util.UUID;

public class InternalArticle
{
    UUID id;
    String internal_id;
    String name;
    BigDecimal price;
    double vat;
    int stock;

    public InternalArticle(String internal_id, String name, BigDecimal price, double vat, int stock) {
        this.id = UUID.randomUUID();
        this.internal_id = internal_id;
        this.name = name;
        this.price = price;
        this.vat = vat;
        this.stock = stock;
    }

    public InternalArticle(InternalArticle IntArt) {
        this.id = IntArt.getId();
        this.internal_id = IntArt.getInternal_id();
        this.name = IntArt.getName();
        this.price = IntArt.getPrice();
        this.vat = IntArt.getVat();
        this.stock = 1;
    }

    public InternalArticle() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getInternal_id() {
        return internal_id;
    }

    public void setInternal_id(String internal_id) {
        this.internal_id = internal_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public double getVat() {
        return vat;
    }

    public void setVat(double vat) {
        this.vat = vat;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void povecajKolicino(int kol)
    {
        this.stock += kol;
    }
}
