package VodenjeRacunov;

import java.math.BigDecimal;
import java.sql.SQLOutput;
import java.util.UUID;

public class Artikel implements Searchable
{
    private UUID id;
    private String ime;
    private BigDecimal cena;
    private double davcnaStopnja;
    private int kolicina;
    private String EAN;
    private String drzava;

    public Artikel(UUID uid, String id, String ime, BigDecimal cena, double davcnaStopnja, String drzava){
        this.EAN = id;
        this.ime = ime;
        this.cena = cena;
        this.davcnaStopnja = davcnaStopnja;
        this.kolicina = 1;
        this.drzava = drzava;
        this.id = uid;
    }

    public Artikel(Artikel a)
    {
        this.EAN = a.getEAN();
        this.ime = a.getIme();
        this.cena = a.getCena();
        this.davcnaStopnja = a.getDavcnaStopnja();
        this.kolicina = 1;
        this.drzava = a.getDrzava();
        this.id = a.getId();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Artikel() {
        this.kolicina = 1;
    }

    public String getEAN() {
        return EAN;
    }

    public void setEAN(String EAN) {
        this.EAN = EAN;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public BigDecimal getCena() {
        return cena;
    }

    public void setCena(BigDecimal cena) {
        this.cena = cena;
    }

    public double getDavcnaStopnja() {
        return davcnaStopnja;
    }

    public void setDavcnaStopnja(double davcnaStopnja) {
        this.davcnaStopnja = davcnaStopnja;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public String getDrzava() {
        return drzava;
    }

    public void setDrzava(String drzava) {
        this.drzava = drzava;
    }

    public void povecajKolicino(int kol)
    {
        this.kolicina += kol;
    }

    @Override
    public String toString() {
        return "Artikel{" +
                "ime='" + ime + '\'' +
                ", cena=" + cena +
                ", davcnaStopnja=" + davcnaStopnja +
                ", kolicina=" + kolicina +
                ", EAN='" + EAN + '\'' +
                ", drzava='" + drzava + '\'' +
                '}';
    }

    @Override
    public boolean search(String s)
    {
        if(this.getEAN().contains(s))
        {
            return true;
        }
        else if(this.ime.contains(s))
        {
            return true;
        }
        else if(this.cena.toString().contains(s))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean checkDigit(String ean, int v)
    {
        int sum = 0;
        int check;
        int stevilo;

        for(int i=0; i<ean.length()-1; i++)
        {
            stevilo = Integer.parseInt(""+ean.charAt(i));
            if(i%2==0)
            {
                sum += stevilo*1;
            }
            else
            {
                sum += stevilo*3;
            }
        }
        check = sum%10;
        check = (check-10)*(-1);

        if(check == v)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean checkDigit14(String ean, int v)
    {
        int sum = 0;
        int check;
        int stevilo;

        for(int i=0; i<ean.length(); i++)
        {
            stevilo = Integer.parseInt(""+ean.charAt(i));
            if(i%2==0)
            {
                sum += stevilo*3;
            }
            else
            {
                sum += stevilo;
            }
        }
        check = sum%10;
        check = (check-10)*(-1);

        if(check == v)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void novaTeza(String teza)
    {
        String koda = this.getEAN();
        char[] nova = koda.toCharArray();
        int j=0;


        if(teza.length() == 5)
        {
            for(int i=7; i<=11; i++)
            {
                nova[i] = teza.charAt(j);
                j++;
            }
            koda = "";
            for(int i=0; i<13; i++)
            {
                koda += nova[i];
            }
            this.setEAN(koda);
        }
        else
        {
            System.out.println("Napaka");
        }
        //Poprava check digita? upraši
    }

    public void razberiEAN()
    {
        String koda = this.getEAN();
        char[] nova = koda.toCharArray();
        String oddelek = "";
        String id = "";
        String teza = "";
        String checkDigit = String.valueOf(koda.charAt(12));

        int i=0;

        while(i < 13)
        {
            if(i <= 2 && i >= 0)
            {
                oddelek += String.valueOf(nova[i]);
            }
            else if(i <= 6 && i >= 3)
            {
                id += String.valueOf(nova[i]);
            }
            else if(i <= 11 && i >= 7)
            {
                teza += String.valueOf(nova[i]);
            }
            i++;
        }
        System.out.println("Oddelek: "+oddelek+"\nId: "+id+"\nTeža: "+teza+"g\nCheckdigit: "+checkDigit);
    }
}
