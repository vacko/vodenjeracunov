package VodenjeRacunov;

import org.apache.commons.dbcp2.BasicDataSource;
import si.um.feri.database.DBHelper;
import si.um.feri.database.UUIDadapter;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.UUID;

public class MySqlCompany implements CompaniesDao
{
    final String SQL_GET_BY_TAX_NUMBER = "SELECT * FROM `company` WHERE `tax_number`=?";
    final String SQL_GET_BY_ID = "SELECT * FROM `company` WHERE hex(`company_id`)=?;";
    final String SQL_GET_ALL = "SELECT * FROM `company`";
    final String SQL_INSERT = "INSERT INTO `company`(`company_id`,`name`,`tax_number`,`registration_number`,`phone_number`,`taxpayer`, `address`, `deleted`, `created`) VALUES(?,?,?,?,?,?,?,?,?)";
    final String SQL_UPDATE = "UPDATE `company` SET `name`=?, `tax_number`=?, `registration_number`=?, `phone_number`=?, `address`=? WHERE `company_id`=?;";
    final String SQL_DELETE = "DELETE FROM `company` WHERE `company_id`=?;";

    @Override
    public Podjetje getByDavcnaStevilka(String davcna) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_BY_TAX_NUMBER);
            prepStat.setString(1, davcna);

            ResultSet resSet = prepStat.executeQuery();

            if(resSet.first())
            {
                return extractFromResultSet(resSet);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Podjetje getById(UUID id) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_BY_ID);
            prepStat.setString(1, id.toString().replace("-", ""));

            ResultSet resSet = prepStat.executeQuery();
            System.out.println(resSet);

            if(resSet.first())
            {
                return extractFromResultSet(resSet);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Podjetje> getAll() {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_GET_ALL);
            ResultSet resSet = prepStat.executeQuery();
            List<Podjetje> podjetje = new ArrayList<>();

            try(ResultSet resultSet = prepStat.executeQuery())
            {
                while (resultSet.next())
                {
                    podjetje.add(extractFromResultSet(resultSet));
                }
                return podjetje;
            }
            catch (Exception e)
            {
                connection.rollback();
                e.printStackTrace();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insert(Podjetje m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_INSERT);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(m.getId()));
            prepStat.setString(2, m.getIme());
            prepStat.setString(3, m.getDavcnaSt());
            prepStat.setString(4, m.getMaticnaSt());
            prepStat.setString(5, m.getTel());
            prepStat.setBoolean(6, m.isJeDavcniZavezanec());
            prepStat.setString(7, m.getNaslov());
            prepStat.setInt(8, 0);
            prepStat.setTimestamp(9, new Timestamp(System.currentTimeMillis()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Podjetje m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_UPDATE);
            prepStat.setString(1, m.getNaslov());
            prepStat.setString(2, m.getDavcnaSt());
            prepStat.setString(3, m.getMaticnaSt());
            prepStat.setString(4, m.getTel());
            prepStat.setBoolean(5, m.isJeDavcniZavezanec());
            prepStat.setString(6, m.getNaslov());
            prepStat.setBytes(7, UUIDadapter.getBytesFromUUID(m.getId()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Podjetje m) {
        try(Connection connection = DBHelper.getConnection())
        {
            PreparedStatement prepStat = connection.prepareStatement(SQL_DELETE);
            prepStat.setBytes(1, UUIDadapter.getBytesFromUUID(m.getId()));

            int i = prepStat.executeUpdate();

            if(i == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Podjetje extractFromResultSet(ResultSet rs) throws SQLException {

        Podjetje podjetje = new Podjetje();
        podjetje.setId(UUIDadapter.getUUIDFromBytes(rs.getBytes(1)));
        podjetje.setIme(rs.getString(2));
        podjetje.setDavcnaSt(rs.getString(3));
        podjetje.setMaticnaSt(rs.getString(4));
        podjetje.setTel(rs.getString(5));
        podjetje.setJeDavcniZavezanec(rs.getBoolean(6));
        podjetje.setNaslov(rs.getString(7));

        return podjetje;
    }
}
