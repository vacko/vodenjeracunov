package VodenjeRacunov;

import com.google.gson.Gson;

import java.text.spi.CollatorProvider;
import java.util.ArrayList;
import java.util.List;

public class Companies implements JsonSupport
{
    private List<Podjetje> seznam;

    public Companies()
    {
        this.seznam = new ArrayList<>();
    }

    public Companies(List<Podjetje> podjetja){this.seznam = podjetja;}

    public List<Podjetje> getSeznamPodjetij() {
        return this.seznam;
    }

    public void setSeznamPodjetij(List<Podjetje> seznam) {
        this.seznam = seznam;
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }

    @Override
    public void fromJson(String s) {
        Gson gson = new Gson();
        Companies c = gson.fromJson(s, Companies.class);
        this.seznam = c.seznam;
    }
}
