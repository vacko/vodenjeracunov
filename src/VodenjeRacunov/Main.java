package VodenjeRacunov;

import com.opencsv.CSVReader;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.StringUtils;
import si.um.feri.database.DBHelper;
import si.um.feri.database.UUIDadapter;

import java.io.FileReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.Inet4Address;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static VodenjeRacunov.Artikel.checkDigit;

public class Main
{
    public static void main(String[] args){

        Podjetje spar = new Podjetje("Spar Slovenija d.o.o.", "32156782", "5571693", true, "015844400", "Letališka cesta 26");
        Podjetje lidl = new Podjetje("Lidl Slovenija d.o.o. k.d.", "59314923", "1719947", true, "0807060", "Pod lipami 1");
        Podjetje hofer = new Podjetje("HOFER trgovina d.o.o.", "91043522", "1992414000", true, "018346600", "Kranjska cesta 1");
        Podjetje tus = new Podjetje("ENGROTUŠ d.o.o.", "87927497", "5494516000", true, "059733000", "Cesta v Trnovlje 10A");
        Podjetje mercator = new Podjetje("MERCATOR, d.d.", "45884595", "5300231000", true, "015601000", "Dunajska cesta 107");

        Artikel domacica = new Artikel(UUID.randomUUID(), "3850102314126", "Domačica keksi", new BigDecimal("3.24"), 9.5, "Hrvaška");
        Artikel skittles = new Artikel(UUID.randomUUID(), "4009900500166", "Skittles fruits", new BigDecimal("1.79"), 9.5, "Nemčija");
        Artikel voda = new Artikel(UUID.randomUUID(), "3830000629782", "Radenska naturelle", new BigDecimal("0.59"), 9.5, "Slovenija");
        Artikel kartusa = new Artikel(UUID.randomUUID(), "4960999974491", "Canon Pixma Black XL", new BigDecimal("5.99"), 22, "Japonska");
        Artikel stolp = new Artikel(UUID.randomUUID(), "3830001640113", "Stolp enka", new BigDecimal("3.99"), 22, "Slovenija");
        Artikel airsoft = new Artikel(UUID.randomUUID(), "35599617120209", "High density BBs", new BigDecimal("6.49"), 22, "Francija");

        Artikel banane = new Artikel(UUID.randomUUID(), "2116789012252", "Banane", new BigDecimal("3.23"), 9.5, "Slovenija");
        Artikel jabolka = new Artikel(UUID.randomUUID(), "2115723005008", "Jabolka", new BigDecimal("1.23"), 9.5, "Slovenija");
        Artikel kivi = new Artikel(UUID.randomUUID(), "2119523001000", "Kivi", new BigDecimal("0.81"), 9.5, "Slovenija");

        Artikli seznam1 = new Artikli();
        List<Racun> seznamRacunov = new ArrayList<>();

        seznam1.dodajArtikel(new Artikel(domacica));
        seznam1.dodajArtikel(new Artikel(skittles));
        seznam1.dodajArtikel(new Artikel(voda));
        seznam1.dodajArtikel(new Artikel(voda));
        seznam1.dodajArtikel(new Artikel(voda));

        Artikli seznam2 = new Artikli();
        seznam2.dodajArtikel(new Artikel(kartusa));
        seznam2.dodajArtikel(new Artikel(stolp));
        seznam2.dodajArtikel(new Artikel(airsoft));
        seznam2.dodajArtikel(new Artikel(airsoft));

        Artikli seznam3 = new Artikli();
        seznam3.dodajArtikel(new Artikel(kivi));
        seznam3.dodajArtikel(new Artikel(jabolka));
        seznam3.dodajArtikel(new Artikel(banane));

        seznamRacunov.add(new Racun(new Date(), seznam1, hofer, hofer.getDavcnaSt()));
        seznamRacunov.add(new Racun(new Date(), seznam2, spar, spar.getDavcnaSt()));
        seznamRacunov.add(new Racun(new Date(), seznam3, lidl, spar.getDavcnaSt()));


        for(Racun racun: seznamRacunov)
        {
            racun.printajRacun();
        }

        System.out.println("Iskanje za besedo keksi: "+domacica.search("keksi"));
        System.out.println("Iskanje za besedo fun: "+skittles.search("fun"));

        System.out.println(domacica.getEAN());
        System.out.println(Artikel.checkDigit(domacica.getEAN(), 6));

        System.out.println("Banane: "+banane.getEAN());
        banane.novaTeza("02500");
        System.out.println("Banane: "+banane.getEAN());

        String tempEan = "0068826713647";
        int tempCheck = 4;

        //System.out.println(Artikel.checkDigit14(tempEan, tempCheck));

        /*//puk
        Kupon kupon1 = new Kupon("9935010620195", "35% popust na račun");
        System.out.println("Kupon");


        seznamRacunov.get(0).setKončnaCena(seznamRacunov.get(0).generirajCeno());
        seznamRacunov.get(0).setKončnaCenaZDDV(seznamRacunov.get(0).generirajCenoZDdv());
        seznamRacunov.get(0).printajRacun();
        try {
            seznamRacunov.get(0).uporabiKupon(kupon1);
            seznamRacunov.get(0).printajRacun();
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        //JSON branje/pisanje
        //Pisanje artiklov v datoteko dat.json
        /*Helper.pisanjeVDatoteko(seznam1.toJson(),"C:\\Users\\Tadej\\Desktop\\Oddane naloge\\2.letnik\\Programski jeziki\\datoteke\\artikli.json");
        String s = Helper.branjeIzDatoteke("C:\\Users\\Tadej\\Desktop\\Oddane naloge\\2.letnik\\Programski jeziki\\datoteke\\artikli.json");
        seznam1.fromJson(s);
        System.out.println(seznam1.getSeznamArtiklov().get(0).getIme());

        //Branje iz datoteke
        s = Helper.branjeIzDatoteke("C:\\Users\\Tadej\\Desktop\\Oddane naloge\\2.letnik\\Programski jeziki\\datoteke\\dat.json");
        System.out.println("Json: "+s);*/

        //TEST BAZE
        /*try(Connection connection = DBHelper.getConnection();
            PreparedStatement prepStat = connection.prepareStatement("SELECT * from SchemaVersion"))//DELA
        {
            //System.out.println("The Connection Object is of Class "+connection.getClass());
            try(ResultSet resultSet = prepStat.executeQuery())
            {
                while(resultSet.next())
                {
                    //get stuff here
                }
            }
            catch (Exception e)
            {
                connection.rollback();
                e.printStackTrace();
            }
        }
        catch (SQLException e)
        {
            System.out.println("SQLException: "+e.toString());
        }*/

        //java.sql.SQLException: Data source is closed
        //DBHelper.insertDataCSV("C:\\Users\\Tadej\\Desktop\\Oddane naloge\\2.letnik\\Programski jeziki\\en.openfoodfacts.org.products.csv", '\t');
        DBHelper.insertDataCSV("C:\\Users\\Tadej\\Desktop\\Oddane naloge\\2.letnik\\Programski jeziki\\Grocery_UPC_Database.csv", ',');

        InternalArticle mleko = new InternalArticle("2723", "Mleko", new BigDecimal("1.09"), 9.5, 100);
        InternalArticle jajca = new InternalArticle("6233", "Jajca", new BigDecimal("1.59"), 9.5, 40);
        InternalArticle krompir = new InternalArticle("1531", "Krompir", new BigDecimal("3.99"), 9.5, 40);
        InternalArticle maslo = new InternalArticle("3451", "Maslo", new BigDecimal("1.79"), 9.5, 70);
        InternalArticle toast = new InternalArticle("1121", "Toast", new BigDecimal("2.19"), 9.5, 20);

        InternalArticleList IntArtList = new InternalArticleList();
        IntArtList.dodajArtikel(new InternalArticle(mleko));
        IntArtList.dodajArtikel(new InternalArticle(jajca));
        IntArtList.dodajArtikel(new InternalArticle(krompir));
        IntArtList.dodajArtikel(new InternalArticle(maslo));
        IntArtList.dodajArtikel(new InternalArticle(toast));

        MySqlInternalArticle sqlInternalArticle = new MySqlInternalArticle();

        sqlInternalArticle.insert(mleko);
        sqlInternalArticle.insert(jajca);
        sqlInternalArticle.insert(krompir);
        sqlInternalArticle.insert(maslo);
        sqlInternalArticle.insert(toast);

        MySqlCompany sqlCompany = new MySqlCompany();
        sqlCompany.insert(mercator);
        sqlCompany.insert(spar);
        sqlCompany.insert(tus);
        sqlCompany.insert(lidl);
        sqlCompany.insert(hofer);

        MySqlArticle sqlArticle = new MySqlArticle();
        Artikli vsiArtikli = new Artikli(sqlArticle.getAll());

        Artikli seznamArtiklov1 = new Artikli();
        seznamArtiklov1.dodajArtikel(new Artikel(vsiArtikli.getSeznamArtiklov().get(2)));
        //ostalo


        MySqlInternalArticle invoice = new MySqlInternalArticle();
        InternalArticleList vsiInternalArticalList = new InternalArticleList(invoice.getAll());

        InternalArticleList racun1 = new InternalArticleList();
        racun1.dodajArtikel(new InternalArticle(vsiInternalArticalList.getList().get(1)));
        //ostalo

        MySqlCompany vsiSqlCompany = new MySqlCompany();
        Companies vsaPodjetja = new Companies(vsiSqlCompany.getAll());

        Podjetje tusNov = new Podjetje(vsaPodjetja.getSeznamPodjetij().get(0));
        Podjetje lidlNov = new Podjetje(vsaPodjetja.getSeznamPodjetij().get(1));
        Podjetje sparNov = new Podjetje(vsaPodjetja.getSeznamPodjetij().get(2));
        Podjetje mercatorNov = new Podjetje(vsaPodjetja.getSeznamPodjetij().get(3));
        Podjetje hoferNov = new Podjetje(vsaPodjetja.getSeznamPodjetij().get(4));

        Podjetje podjetje = new Podjetje(vsiSqlCompany.getById(hoferNov.getId()));

        System.out.println(podjetje.getIme());

        Racun novRacun = new Racun(seznamArtiklov1, racun1, hoferNov);

        MySqlInvoice racun = new MySqlInvoice();
        racun.create_invoice(novRacun);
    }
}
