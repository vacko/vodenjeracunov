package VodenjeRacunov;

public interface ArticleDao extends DaoCrud<Artikel>
{
    Artikel getByBarcode(String code);
}
